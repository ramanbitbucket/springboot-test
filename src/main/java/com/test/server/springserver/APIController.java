package com.test.server.springserver;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

@CrossOrigin
/**
 * RestController
 */
public class APIController {
    @GetMapping(value = "/api/byID")
    public String callApi(@RequestParam(name = "id") String id) {
        return "Response from the server @ " + new java.util.Date() +", ID= " + id;
    }

    @RequestMapping(value = "/api/ticker/{ticker}")
    public String getTickerInfo(@PathVariable String ticker) {
        return "Response from the server @ " + new java.util.Date() +", ticker= " + ticker;
    }
    
}
